class Render extends KonvaManager {
    constructor() {
        super();
    }

    async selectWall(climbId, width, height) {
        const result = await fetch(`/walls/climb/${climbId}`, {
            method: "GET",
            headers: {"Content-type": "application/json; charset=UTF-8"}
        });
        const wall = await result.json();
        if (wall.routes) {
            this.routes.routes = wall.routes;
            this.drawExistingAnchors(this.routes.routes)
        }
        this.addWall(width, height)
        console.log(climbId, wall.routes.filter(route=>route.mysql_id===parseInt(climbId)))
        this.drawRouteLines(climbId, wall.routes.filter(route=>route.mysql_id===parseInt(climbId))[0].coords,
            wall.routes.filter(route=>route.mysql_id===parseInt(climbId))[0].colour)
    }

    drawExistingAnchors(routes) {
        routes.forEach((route) => {
            route.coords.forEach(anchor => {
                const circle = new Konva.Circle({
                    ...anchor,
                    radius: 3,
                    fill: route.colour,
                    stroke: 'black',
                    strokeWidth: 1,
                })
                this.layer.add(circle)
            })
        })
    }

    addWall(width, height) {
        const wallHeight = this.stage.height();
        const wallWidth = (this.stage.height()) / height * width;
        // Create wall
        const wall = new Konva.Rect({
            x: this.stage.width() / 2 - wallWidth / 2,
            y: 0,
            width: wallWidth,
            height: wallHeight,
            fill: 'grey',
            stroke: 'black',
            strokeWidth: 1,
            transform: false
        });
        // add wall to layer
        this.background.add(wall);
    }
}

const render = new Render();
