async function createWall(name, width, height, incline) {
    const result = await fetch(`/walls/create`, {
        method: "POST",
        headers: {"Content-type": "application/json; charset=UTF-8"},
        body: JSON.stringify({name, width, height, incline})
    })
    if (await result.text() === 'OK') {
        window.location.href = '/admin/climbs/create.html'
    }
}

$("#submit").click(async () => {
    // Validate
    const form = {
        height: $("#height").val(),
        width: $("#width").val(),
        name: $("#name").val(),
        incline: $("#incline").val()
    };
    if (form.height && form.width && form.name) {
    console.log(form)
        await createWall(form.name, form.width, form.height, form.incline)
    }
})
