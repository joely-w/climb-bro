class Wall extends KonvaManager {
    constructor() {
        super();
        this.walls = [];
        $("#next").click(async () => {
            $("#wall-chooser").hide();
            $("#konva-wall").show();
            await this.selectWall(parseInt($("#walls").val()))
        });
        $("#grade").on('change', e => {
            this.updateGrade($(e.target).val())
        })
        $("#disipline").on('change', e => {
            this.updateDiscipline($(e.target).val())
        })
        $(document).on("click", ".router", (e) => {
            const element = $(e.target);
            this.currentRouteID = this.routes.routes[element.data('route')].id;
            this.loadRouteDetails();
            this.drawRouteLines(this.routes.routes[element.data('route')].id, this.routes.routes[element.data('route')].coords,
                this.routes.routes[element.data('route')].colour)
        })
        $("#plotting").click(() => {
            if (this.routes.plotting) {
                const routeHeader = this.routes.routes.length - 1;
                $("#routes").append(`<li>
                                        <a href="#" data-route="${routeHeader}" class="router" style="color: ${this.routes.routes[routeHeader].colour}">
                                            Route ${this.routes.routes[routeHeader].id}
                                        </a>
                                     </li>`);
                $("#plotting").html('Start plotting route');
                this.routes.plotting = false;
            } else {
                this.stage.find('Line').forEach(node => node.destroy())
                $("#plotting").html('Stop plotting route');
                this.routes.plotting = true;
                this.routes.colour = $("#colour").val()
                this.currentRouteID = this.generateID();
                this.routes.routes.push({
                    id: this.currentRouteID,
                    colour: this.routes.colour,
                    grade: 650,
                    discipline: 0,
                    coords: []
                })
                this.loadRouteDetails();
            }
        })
        $("#submit").click(async () => {
            const result = await fetch('/walls/update/', {
                method: "POST",
                headers: {"Content-type": "application/json; charset=UTF-8"},
                body: JSON.stringify({
                    id: this.wall.wall_id,
                    routes: this.routes.routes
                })
            })
            if (await result.text() === 'OK') {
                window.location.href = '/admin/climbs/create.html'
            }

        })
    }

    loadRouteDetails() {
        const route = this.routes.routes.find(route => route.id === this.currentRouteID);
        $("#grade").val(route.grade);
        $("#disipline").val(route.discipline)
    }

    updateGrade(newGrade) {
        const index = this.routes.routes.findIndex(route => route.id === this.currentRouteID);
        this.routes.routes[index].grade = newGrade;
    }

    updateDiscipline(value) {
        const index = this.routes.routes.findIndex(route => route.id === this.currentRouteID);
        this.routes.routes[index].discipline = value;
    }

    async getWalls() {
        const walls = await fetch('/get_walls', {
            method: "POST",
            headers: {"Content-type": "application/json; charset=UTF-8"}
        });
        this.walls = await walls.json();
        this.setWalls(this.walls);
    }

    setWalls(walls) {
        walls.forEach(wall => {
            $("#walls").append($('<option>', {value: wall.wall_id, text: wall.wall_name, 'data-width': wall.width}))
        })
    }

    /**
     * Select wall based off ID
     * @param wallId
     */
    async selectWall(wallId) {
        this.wall = this.walls.find(wall => wall.wall_id === wallId);
        $("#title").html(`Wall: ${this.wall.wall_name}`)
        const result = await fetch(`/walls/${wallId}`, {
            method: "GET",
            headers: {"Content-type": "application/json; charset=UTF-8"}
        });
        const wall = await result.json();
        if (wall.routes) {
            this.routes.routes = wall.routes;
            this.drawExistingAnchors(this.routes.routes)

        }
        this.addWall(this.wall.width, this.wall.height)
    }

    drawExistingAnchors(routes) {
        routes.forEach((route, index) => {
            $("#routes").append(`<li>
                                        <a href="#" data-route="${index}" class="router" style="color: ${route.colour}">
                                            Route ${route.id}
                                        </a>
                                     </li>`);
            route.coords.forEach(anchor => {
                const circle = new Konva.Circle({
                    ...anchor,
                    radius: 3,
                    fill: route.colour,
                    stroke: 'black',
                    strokeWidth: 1,
                })
                this.layer.add(circle)
            })
        })
    }

    /**
     * Add a wall given width and height
     * @param width
     * @param height
     */
    addWall(width, height) {
        const wallHeight = this.stage.height();
        const wallWidth = this.stage.height() / height * width;
        // Create wall
        const wall = new Konva.Rect({
            x: this.stage.width() / 2 - wallWidth / 2,
            y: 0,
            width: wallWidth,
            height: wallHeight,
            fill: 'grey',
            stroke: 'black',
            strokeWidth: 1,
            transform: false
        });
        // add wall to layer
        this.background.add(wall);
    }


}

const wall = new Wall()

$(async () => {
    await wall.getWalls()
})
