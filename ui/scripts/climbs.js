class Climbs {


    get_home_gym() {
        fetch('/get_home_gym', {
            method: "POST",
            headers: {"Content-type": "application/json; charset=UTF-8"}
        }).then(response => response.json()).then(json => {
            console.log(json);
            if (!json) {
                window.location.assign("login.html");
            } else {
                let gym_id = json[0]["home_gym"];
                $("#gym").val(gym_id);
                this.fetch_walls()
            }
        });
    }

    fetch_gyms() {
        fetch('/get_gyms', {
            method: "POST",
            headers: {"Content-type": "application/json; charset=UTF-8"}
        }).then(response => response.json()).then(json => {
            console.log(json);
            if (json != false) {
                json.forEach(element => {
                    $("#gym").append($('<option>', {
                        value: element["gym_id"],
                        text: element["gym_name"]
                    }));
                });
            }
            this.fetch_walls()
        });
    }

    fetch_walls() {
        $("#wall").empty();
        $("#wall").append($('<option>', {
            text: "Select a wall...",
            hidden: true,
            default: true,
            value: "'%'"
        }));
        if (!$("#gym").val() || $("#gym").val() === "'%'") {
            console.log('No gym selected');
            return
        }
        fetch('/get_walls', {
            method: "POST",
            body: JSON.stringify({"gym_id": $("#gym").val()}),
            headers: {"Content-type": "application/json; charset=UTF-8"}
        }).then(response => response.json()).then(json => {
            console.log(json);
            if (json != false) {
                json.forEach(element => {
                    $("#wall").append($('<option>', {
                        value: element["wall_id"],
                        text: element["wall_name"]
                    }));
                });
            }
        });
        this.fetch_climbs();
    }

    calculate_expected(discipline, type, callback) {
        fetch('/calc_q', {
            method: "POST",
            body: JSON.stringify({"discipline": discipline, "type": type}),
            headers: {"Content-type": "application/json; charset=UTF-8"}
        }).then(response => response.json()).then(json => {
            return callback(json["q"]);

        });
    }

    convert_grade(gradeNum) {
        let number = gradeNum.toString().substr(0, 1);
        let conversion = {
            "00": "a",
            "17": "a+",
            "34": "b",
            "50": "b+",
            "67": "c",
            "83": "c+"
        };
        let letter = conversion[gradeNum.toString().substr(1, 2)];
        return number.toString() + letter;
    }

    fetch_climbs() {
        let datas = {
            "gym": $("#gym").val(),
            "grade": $("#grade").val(),
            "wall": $("#wall").val(),
            "difficulty": $("#difficulty").val(),
            "disipline": $("#disipline").val(),
            "type": $("#type").val()
        };
        fetch('/get_climbs', {
            method: "POST",
            body: JSON.stringify(datas),
            headers: {"Content-type": "application/json; charset=UTF-8"}
        }).then(response => response.json()).then(json => {
            console.log(json);
            if (json === false) {
                console.log("No climbs found");
            } else {
                const ordered = Object.keys(json).sort().reduce(
                    (obj, key) => {
                        obj[key] = json[key];
                        return obj;
                    },
                    {}
                );

                console.log(ordered);
                for (const element in ordered) {
                    $("#climbsList").append(`<li>` + this.convert_grade(ordered[element]["grade"]) + "     " + ordered[element]["displine_name"] + ` climb.
                        <button onclick="window.location.assign('logClimb.html?climbId=` + ordered[element]['climb_id'] + `') " class="button is-info">View Climb</button></li>`);
                }

            }
        });
    }

    async get_climb_data(climb_id) {
        const response = fetch('/get_climb_data', {
            method: "POST",
            body: JSON.stringify({"climb_id": climb_id}),
            headers: {"Content-type": "application/json; charset=UTF-8"}
        }).then(response => response.json());
        return await response;

    }

    submit_climb(climb_id, difficulty, enjoyment, category, callback) {
        fetch('/submit_climb', {
            method: "POST",
            body: JSON.stringify({
                "climb_id": climb_id, "difficulty": difficulty,
                "enjoyment": enjoyment, "category": category
            }),
            headers: {"Content-type": "application/json; charset=UTF-8"}
        }).then(response => response.json()).then(json => {
            console.log(json);
            return callback(json);
        });
    }

    get_user_average(user_id) {
        fetch('/get_user_average', {
            method: "POST",
            body: JSON.stringify({"user_id": user_id}),
            headers: {"Content-type": "application/json; charset=UTF-8"}
        }).then(response => response.json()).then(json => {
            console.log(json);
        });
    }

    get_adjusted_grade(climb_id, user_id, callback1) {
        fetch('/get_adjusted_grade', {
            method: "POST",
            body: JSON.stringify({"user_id": user_id, "climb_id": climb_id}),
            headers: {"Content-type": "application/json; charset=UTF-8"}
        }).then(response => response.json()).then(json => {
            console.log(json);
            return callback1(json);
        });
    }

    get_completed_climbs() {
        fetch('/get_completed_climbs', {
            method: "POST",
            headers: {"Content-type": "application/json; charset=UTF-8"}
        }).then(response => response.json()).then(json => {
            let ordered = json;
            for (const element in json) {
                $("#climbsList").append(`<li>` + this.convert_grade(ordered[element]["grade"]) + `    ` + ordered[element]["displine_name"] + ` climb, on ` + ordered[element]['log_date'] + `
                        <button onclick="window.location.assign('logClimb.html?climbId=` + ordered[element]['climb_id'] + `') " class="button is-info">View Climb</button></li>`);
            }
        });
    }

}
