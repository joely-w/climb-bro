const container = $("#konva-container")

class KonvaManager {
    static dimensions = {
        width: container.width(),
        height: container.height()
    }

    constructor() {
        console.log(`Starting Konva with dimensions: ${KonvaManager.dimensions.width}x${KonvaManager.dimensions.height}`)
        this.stage = new Konva.Stage({
            container: 'konva-container',
            ...KonvaManager.dimensions,
            draggable: true
        });
        this.routes = {plotting: false, routes: []}

        this.background = new Konva.Layer()
        this.layer = new Konva.Layer()

        this.initHandlers();

        this.stage.add(this.background)
        this.stage.add(this.layer);

    }

    initHandlers() {
        this.stage.on('click tap', (e) => this.click(e));
        var scaleBy = 1.2;
        this.stage.on('wheel', (e) => {
            e.evt.preventDefault();
            const oldScale = this.stage.scaleX();

            const pointer = this.stage.getPointerPosition();

            const mousePointTo = {
                x: (pointer.x - this.stage.x()) / oldScale,
                y: (pointer.y - this.stage.y()) / oldScale,
            };

            const newScale =
                e.evt.deltaY > 0 ? oldScale * scaleBy : oldScale / scaleBy;

            this.stage.scale({x: newScale, y: newScale});

            const newPos = {
                x: pointer.x - mousePointTo.x * newScale,
                y: pointer.y - mousePointTo.y * newScale,
            };
            this.stage.position(newPos);
        });
    }

    addAnchor(e) {
        const currentRoutePointer = this.routes.routes.length - 1;
        const circle = new Konva.Circle({
            x: (-this.stage.x() + e.evt.offsetX)/this.stage.scaleX(),
            y: (-this.stage.y() + e.evt.offsetY)/this.stage.scaleY(),
            radius: 3,
            fill: this.routes.colour,
            stroke: 'black',
            strokeWidth: 1,
        })
        this.layer.add(circle);
        this.layer.draw();
        this.routes.routes[currentRoutePointer].coords.push({x: (e.evt.offsetX-this.stage.x())/this.stage.scaleX(), y:( e.evt.offsetY-this.stage.y())/this.stage.scaleY()})
        this.drawRouteLines(this.routes.routes[currentRoutePointer].id, this.routes.routes[currentRoutePointer].coords, this.routes.routes[currentRoutePointer].colour)
    }

    generateID() {
        if (!this.routes.routes.length) return 0
        return Math.max.apply(Math, this.routes.routes.map((o) => o.id)) + 1
    }

    drawRouteLines(routeId, anchors, stroke = 'red') {
        this.stage.find('Line').forEach(node => node.destroy())
        for (let i = 0; i < anchors.length - 1; i++) {
            const line = new Konva.Line({
                stroke,
                name: `${routeId + 1}`,
                points: [anchors[i].x, anchors[i].y, anchors[i + 1].x, anchors[i + 1].y]
            });
            this.layer.add(line);
        }
    }

    /**
     * Generic handlers to be put here
     */
    click(e) {
        // if click on empty area - remove all selections
        if (e.target === this.stage || e.target.attrs.transform === false) {
            if (this.routes.plotting) {
                this.addAnchor(e)
            }
        }
    }
}
