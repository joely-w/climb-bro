class Register{

    register(name, password, gym){
        fetch('/register', {
            method: "POST",
            body: JSON.stringify({"name":name, "password": password, "gym":gym }),
            headers: {"Content-type": "application/json; charset=UTF-8"}
        }).then(response => response.json()) .then(json => {
            if(json==true){
                window.location.assign("login.html");
            }
            else{
                console.log(json);
                alert("Please check your details and try again");
            }
        });

    }

}

$(document).ready(function(){
    $("#submitBtn").click(function(){
        let register = new Register();
        register.register($("#username").val(), $("#password").val(), $("#gym").val());
    });
});


