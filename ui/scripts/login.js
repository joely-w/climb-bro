class Login{

    login(name, password){
        fetch('/login', {
            method: "POST",
            body: JSON.stringify({"name":name, "password": password}),
            headers: {"Content-type": "application/json; charset=UTF-8"}
        }).then(response => response.json()) .then(json => {
            console.log(json);
            if(json.success===true){
                window.location.assign("index.html");
            }
            else{
                alert("Please check your details and try again");
            }
        });

    }

}

$(document).ready(function(){
    $("#loginBtn").click(function(){
        let login = new Login();
        login.login($("#username").val(), $("#password").val());
    });
});
