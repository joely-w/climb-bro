const express = require('express')

const app = express();
const port = 8080; // default port to listen
app.use(express.static('../ui'));
const session = require("express-session");
app.use(session({secret: 'ssshhhhh', resave: true, saveUninitialized: true}));

let sess;
const dbConn = new (require("./services/database.js"))();
const climbs = new (require("./services/climbs.js"))();
const routes = new (require('./services/routes'))();
const bodyParser = require('body-parser');
const walls = new (require("./services/walls"))();

// create application/json parser
const jsonParser = bodyParser.json();

// create application/x-www-form-urlencoded parser
const urlencodedParser = bodyParser.urlencoded({extended: false});
app.use((req, res, next) => {
    if (req.url === '/register' || req.url === '/login') return next();
    if (!req?.session?.userid) return res.status(401);
    return next();
})
app.post('/register', jsonParser, function (request, response) {
    dbConn.insert_new_user(request.body, function (responseData) {
        response.send(responseData);
    });
});

app.post('/login', jsonParser, function (request, response) {
    let sess = request.session;
    dbConn.check_user(request.body, function (responseData) {
        if (responseData.length >= 1) {
            sess.userid = responseData[0]["user_id"];
            sess.loggedInStatus = true;
            response.send({"success": true});
        } else {
            response.send({"success": false});
        }
    });
});

app.post('/calc_q', jsonParser, async function (request, response) {
    response.send(await climbs.calc_q(request.body.discipline, request.body.type, request.session.userid));
});

app.post('/get_climb_data', jsonParser, async function (request, response) {
    response.send(await climbs.get_climb_data(request.body.climb_id));
});

app.post('/get_user_average', jsonParser, async function (request, response) {
    response.send(await climbs.get_user_average(request.body.user_id));
});

app.post('/sign_out', jsonParser, async function (request, response) {
    request.session.userid == null;
});

app.post('/get_adjusted_grade', jsonParser, async function (request, response) {
    const res = await climbs.get_adjusted_grade(request.body.user_id, request.body.climb_id);
    response.send(res)
});


app.post('/submit_climb', jsonParser, async function (request, response) {
    let data = request.body;
    response.send(await climbs.submit_climb(request.session.userid, data.climb_id, data.difficulty, data.enjoyment, data.category));
});

app.post('/get_home_gym', jsonParser, function (request, response) {
    climbs.get_home_gym(request.session.userid, function (responseData) {
        response.send(responseData);
    });
});

app.post('/get_completed_climbs', jsonParser, function (request, response) {
    climbs.get_completed_climbs(request.session.userid, function (responseData) {
        response.send(responseData);
    });
});


app.post('/get_climbs', jsonParser, async function (request, response) {
    const res = await climbs.fetch_climbs(request.body, request.session.userid);
    console.log("res: " + res);
    response.send(res);
})

app.post('/get_gyms', jsonParser, function (request, response) {
    climbs.fetch_gyms(function (responseData) {
        response.send(responseData);
    });
})

app.post('/get_walls', jsonParser, function (request, response) {
    console.log(`Fetching walls with ${request.body.gym_id}`)
    if (request.body.gym_id) {
        climbs.fetch_walls(request.body.gym_id, function (responseData) {
            response.send(responseData);
        });
    } else {
        climbs.get_home_gym(request.session.userid, (value) => {
            climbs.fetch_walls(value[0]['home_gym'], function (responseData) {
                response.send(responseData);
            });
        })
    }

});

app.get('/status', function (req, res) {
    res.send("yolo");
});

app.post('/walls/create', jsonParser, async (req, res) => {
    climbs.get_home_gym(req.session.userid, async val => {
        if (!val?.length) return;
        await walls.createWall(req.body.name, req.body.width, req.body.height, req.body.incline, val[0]['home_gym'], async (result) => {
            await routes.createWall(result);
            return res.sendStatus(200)
        })
    })
})
app.get('/walls/:wallId', async (req, res) => {
    res.send(await walls.wall(req.params.wallId))
})
app.get('/walls/climb/:climbId', async (req, res) => {
    res.send(await walls.wallFromClimb(req.params.climbId));
})
app.get('/walls/', async (req, res) => {
    climbs.get_home_gym(req.session.userid, async val => {
        if (!val.length) val = [{home_gym: 1}];
        const result = await walls.getWalls(val[0]['home_gym']);
        console.log(result)
        res.send(result)
    })
})
app.post('/walls/update', jsonParser, async (req, res) => {
    await routes.updateWall(req.body.id, req.body.routes);

    res.sendStatus(200);
})
// start the express server
app.listen(port, async () => {
    if (!routes.db) await routes.connect();
    console.log(`server started at http://localhost:${port}`);
});
