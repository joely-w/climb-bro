const Database = require('./database')
const routes = new (require('./routes'))();
module.exports = class Walls extends Database {
    constructor() {
        super();
    }

    async createWall(name, width, height, incline, gym_id, cb) {
        await Database.conn.query(`INSERT INTO walls(wall_name, width, height, angle, gym_id)
                                   VALUES ('${name}',
                                           ${parseInt(width)},
                                           ${parseInt(height)},
                                           ${parseInt(incline)},
                                           ${parseInt(gym_id)})`, (err, result, fields) => {
            cb(result.insertId)
        })
    }

    async wall(id) {
        return await routes.getWall(id)
    }

    async wallFromClimb(id) {
        const wallId = (await Database.asyncQuery(`SELECT wall_id
                                                  FROM climbs
                                                  WHERE climb_id = ${parseInt(id)}`))[0].wall_id;
        return await routes.getWall(wallId)
    }

    async wallAngle(id) {
        const result = await Database.asyncQuery(`SELECT angle
                                                  FROM walls
                                                  WHERE wall_id = '${id}'`);
        return result[0]
    }

    async getWalls(gym) {
        return await Database.conn.asyncQuery(`SELECT *
                                          FROM walls
                                          WHERE gym_id = '${gym}'`)
    }
}
