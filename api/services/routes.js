const {MongoClient} = require('mongodb');

require('dotenv').config();
const uri = `mongodb+srv://${process.env['MONGO_USERNAME']}:${process.env['MONGO_PASSWORD']}@climbbro.kvyzm.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`;
const client = new MongoClient(uri, {useNewUrlParser: true, useUnifiedTopology: true});
module.exports = class Routes {
    constructor() {
    }

    async connect() {
        this.db = (await client.connect()).db('climbBro');
    }

    async updateWall(wallId, routes) {
        if (!this.db) await this.connect();
        const climbs = new (require("./climbs.js"))();

        const new_routes = [];
        for (let i = 0; i < routes.length; i++) {
            new_routes.push(await climbs.create_climb(wallId, routes[i]))
        }
        await this.db.collection('walls').updateOne({wall_id: wallId}, {
                $set: {routes: new_routes}
            }
        ).catch(e => console.log(e))
    }

    async createWall(wallId) {
        if (!this.db) await this.connect();
        await this.db.collection('walls').insertOne({wall_id: parseInt(wallId)})
    }

    async getWall(wall_id) {
        if (!this.db) await this.connect();
        return await this.db.collection('walls').findOne({wall_id: parseInt(wall_id)})
    }
};
