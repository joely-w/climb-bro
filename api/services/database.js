const mysql = require('mysql');
require('dotenv').config();
let sanitizer = require("sanitizer");
let md5 = require("md5");
const util = require('util')

module.exports = class Database {
    //database connection files - local server must be configured with these details
    static conn;
    static host = process.env["DB_HOST"];
    static user = process.env["DB_USERNAME"];
    static password = process.env["DB_PASSWORD"];

    constructor() {
        Database.conn = mysql.createConnection({
            host: Database.host,
            user: Database.user,
            password: Database.password,
            multipleStatements: true
        });

        Database.asyncQuery = util.promisify(Database.conn.query).bind(Database.conn)

        Database.conn.connect(function (err) {
            if (err) throw err;
            console.debug("Connected!");
        });
        this.create_database();
    }

    execute_query(sql) {
        Database.conn.query(sql, function (err, result, fields) {
            if (err) throw err;
            //console.log(result);
        });
    }


    create_database() {
        let sql = `CREATE DATABASE IF NOT EXISTS climbbro;
                    USE climbbro;
                    CREATE TABLE IF NOT EXISTS gyms(
                        gym_id INTEGER PRIMARY KEY AUTO_INCREMENT,
                        gym_name VARCHAR(255),
                        location VARCHAR(255)
                    );
                    
                    CREATE TABLE IF NOT EXISTS users(
                        user_id INTEGER PRIMARY KEY AUTO_INCREMENT,
                        name VARCHAR(255),
                        password VARCHAR(255),
                        home_gym INTEGER,
                        FOREIGN KEY (home_gym) REFERENCES gyms(gym_id),
                        d00 INTEGER DEFAULT 1,
                        d01 INTEGER DEFAULT 1,
                        d02 INTEGER DEFAULT 1,
                        d10 INTEGER DEFAULT 1,
                        d11 INTEGER DEFAULT 1,
                        d12 INTEGER DEFAULT 1,
                        d20 INTEGER DEFAULT 1,
                        d21 INTEGER DEFAULT 1,
                        d22 INTEGER DEFAULT 1,
                        e00 INTEGER DEFAULT 1,
                        e01 INTEGER DEFAULT 1,
                        e02 INTEGER DEFAULT 1,
                        e10 INTEGER DEFAULT 1,
                        e11 INTEGER DEFAULT 1,
                        e12 INTEGER DEFAULT 1,
                        e20 INTEGER DEFAULT 1,
                        e21 INTEGER DEFAULT 1,
                        e22 INTEGER DEFAULT 1
                    );
                    
                    CREATE TABLE IF NOT EXISTS gym_staff(
                        staff_id INTEGER PRIMARY KEY AUTO_INCREMENT,
                        gym INTEGER,
                        FOREIGN KEY (gym) REFERENCES gyms(gym_id)
                    );
                    
                    
                    CREATE TABLE IF NOT EXISTS climb_disipline(
                        displine_id INTEGER PRIMARY KEY, 
                        displine_name VARCHAR(255)
                    );
                               
                    
                    CREATE TABLE IF NOT EXISTS walls(
                        wall_id INTEGER PRIMARY KEY AUTO_INCREMENT,
                        wall_name VARCHAR(255),
                        height INTEGER,
                        width INTEGER,
                        angle INTEGER,
                        gym_id INTEGER,
                        FOREIGN KEY (gym_id) REFERENCES gyms(gym_id)
                    );
                    
                    CREATE TABLE IF NOT EXISTS climbs(
                        climb_id INTEGER PRIMARY KEY AUTO_INCREMENT,
                        wall_id INTEGER,
                        angle INTEGER,
                        grade INTEGER,
                        category INTEGER,
                        FOREIGN KEY (wall_id) REFERENCES walls(wall_id),
                        FOREIGN KEY (category) REFERENCES climb_disipline(displine_id) 
                    );
                    
                    CREATE TABLE IF NOT EXISTS finish_categories(
                        cat_id INTEGER PRIMARY KEY,
                        cat_name VARCHAR(255)
                    );
                    
                    CREATE TABLE IF NOT EXISTS climb_log(
                        log_id INTEGER PRIMARY KEY AUTO_INCREMENT,
                        user_id INTEGER,
                        climb_id INTEGER,
                        log_date VARCHAR(255),
                        difficulty INTEGER, 
                        category INTEGER,
                        enjoyment INTEGER                        
                    );`;

        this.execute_query(sql);
    }

    sanitize_inputs(input_array) {
        for (const field in input_array) {
            input_array[field] = sanitizer.sanitize(input_array[field]);
        }
        return input_array;
    }

    insert_new_user(data, callback) {
        data = this.sanitize_inputs(data);
        data["password"] = md5(data["password"]);
        Database.conn.query("INSERT INTO users (name,  password, home_gym) VALUES " +
            "('" + data['name'] + "', '" + data['password'] + "', '" + data['gym'] + "');", function (err) {
            if (err) {
                return callback(false);
            } else {
                return callback(true);
            }
        });
    }

    check_user(data, callback) {
        data = this.sanitize_inputs(data);
        let password = md5(data["password"]);
        let name = data["name"];
        Database.conn.query("SELECT * FROM users WHERE name='" + name + "' AND password='" + password + "'", function (err, result1) {
            if (err) throw err;
            if (result1.length > 0) {
                return callback(result1);
            } else {
                return callback(false);
            }
        });
    }


};
