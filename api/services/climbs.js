let Database = require("./database.js");
const walls = new (require('./walls'))()

class Climbs extends Database {
    constructor() {
        super();
    }

    async create_climb(wallId, route) {
        const new_route = route;
        const wall = await walls.wallAngle(wallId);
        if (!route.mysql_id) {
            const result = await Database.asyncQuery(`INSERT INTO climbs(wall_id, angle, grade, category)
                                                      VALUES (${wallId}, ${wall.angle}, ${route.grade},
                                                              ${route.discipline}) `);
            new_route['mysql_id'] = result.insertId;
        } else {
            await Database.asyncQuery(`UPDATE climbs
                                       SET grade   = ${route.grade},
                                           category=${route.discipline}
                                       WHERE climb_id = ${route.mysql_id}`);
        }
        return new_route;
    }

    async fetch_climbs(data, userId) {
        console.log("****DATA****");
        console.log(data);

        let sql =
            "SELECT * FROM climbs INNER JOIN walls\n" +
            "ON climbs.wall_id = walls.wall_id INNER JOIN gyms " +
            "ON walls.gym_id = gyms.gym_id INNER JOIN climb_disipline " +
            "ON climbs.category = climb_disipline.displine_id " +
            "WHERE gyms.gym_id LIKE " + data["gym"] +
            " AND climbs.grade LIKE  " + data["grade"] +
            " AND climbs.wall_id LIKE " + data["wall"] +
            " AND climbs.category LIKE  " + data["difficulty"];

        //"AND climbs.angle > 1" + data["gym"] +
        if (data["type"] != "%") {
            if (data["type"] == 0) {
                sql += " AND walls.angle < 0";
            } else if (data["type"] == 1) {
                sql += " AND walls.angle > 0";
            } else if (data["type"] == 2) {
                sql += " AND walls.angle = 0";
            }
        }
        const result = await Database.asyncQuery(sql);
        console.log("RESULT:");
        console.log(result);
        const avg = await this.get_user_average(userId);
        console.log(avg);
        let climbs = {};
        if (result.length < 1) {
            return false;
        }


        for (let i = 0; i < result.length; i++) {
            const weightVal = await this.get_adjusted_grade(userId, result[i]["climb_id"]);
            console.log(`Weightval: ${weightVal}`);
            const enjoymentVal = await this.calc_e(result[i]["displine_id"], result[i]["category"], userId);
            console.log(enjoymentVal);
            console.log(weightVal["grade"]);
            let diff = Math.abs(avg[0]["avg"] - weightVal["grade"]) * (1/enjoymentVal);
            console.log(diff);
            while (diff in climbs) diff++;
            climbs[diff] = result[i];
        }

        return climbs;
    }

    fetch_gyms(callback) {
        Database.conn.query("SELECT * FROM gyms", function (err, result) {
            return callback(result);
        })
    }

    fetch_walls(gym_id, callback) {
        const sql = `SELECT * FROM walls WHERE gym_id = ${gym_id}`;
        Database.conn.query(sql, function (err, result) {
            if(err) throw err;
            return callback(result);
        });
    }

    async fetch_wall(wall_id) {
        return await Database.asyncQuery(`SELECT *
                                          FROM walls
                                          WHERE wall_id = ${wall_id}`)
    }

    get_home_gym(user_id, callback) {
        Database.conn.query(`SELECT home_gym
                             FROM users
                             WHERE user_id = '${user_id}'`, function (err, result) {
            if (err) throw err;
            if (result.length > 0) {
                return callback(result);
            } else {
                return callback(false);
            }
        });
    }

    async calc_q(discipline, type, user_id) {
        const result = await Database.asyncQuery(`SELECT *
                                                  FROM users
                                                  WHERE user_id = '${user_id}'`);

        return {"q": result[0]["d" + discipline.toString() + type.toString()]};
    }

    async calc_e(discipline, type, user_id) {
        const result = await Database.asyncQuery(`SELECT *
                                                  FROM users
                                                  WHERE user_id = '${user_id}'`);
        const string = "e" + discipline.toString() + type.toString();
        return result[0][string];
    }

    async submit_climb(user_id, climb_id, difficulty, enjoyment, finish, callback) {
        let date = new Date().toISOString().slice(0, 19).replace('T', ' ');
        await Database.asyncQuery(`INSERT INTO climb_log (user_id, climb_id, log_date, difficulty, category, enjoyment)
                                                  VALUES (` + user_id + `, ` + climb_id + `, '` + date + `', ` + difficulty + `, ` + enjoyment + `, ` + finish + `)             `);
        const climbData = (await this.get_climb_data(climb_id))[0];
        let discipline = climbData["category"];
        let type;
        if (climbData["angle"] < 0) {
            type = 0;
        } else if (climbData["angle"] > 0) {
            type = 1;
        } else {
            type = 2;
        }
        const res2 = await this.calc_q(discipline, type, user_id);
        const res3 = await this.calc_e(discipline, type, user_id);
        let currentWeighting = res2["q"] * climbData["grade"];

        const userStats = (await this.get_user_average(user_id))[0]
        console.log(res2,res3,currentWeighting,userStats)
        let newWeighting = (100/userStats.count) * (difficulty / 5 * climbData["grade"] - currentWeighting * userStats["avg"]) + currentWeighting;
        let newERating = ((enjoyment - res3) / parseInt(userStats.count)) + res3;
        let e = await Database.asyncQuery("UPDATE users SET d" + discipline.toString() + type.toString() + " = " + newWeighting + " WHERE user_id = " + user_id)
        return await Database.asyncQuery("UPDATE users SET e" + discipline + type + " = " + newERating + " WHERE user_id = " + user_id)
    }

    get_completed_climbs(user_id, callback) {
        Database.conn.query("SELECT * FROM climb_log INNER JOIN climbs ON climb_log.climb_id = climbs.climb_id INNER JOIN climb_disipline ON climbs.category = climb_disipline.displine_id WHERE user_id = " + user_id, function (err, result) {
            console.log(result);
            return callback(result);
        });
    }

    async get_climb_data(climb_id) {
        return await Database.asyncQuery(`SELECT *
                                          FROM climbs
                                                   INNER JOIN walls ON climbs.wall_id = walls.wall_id
                                          WHERE climbs.climb_id = ` + climb_id + `;`);
    }

    async get_user_average(user_id) {
        return await Database.asyncQuery(`SELECT AVG(climbs.grade) as avg, COUNT(*) as count
                                          FROM climbs
                                                   INNER JOIN climb_log ON climbs.climb_id = climb_log.climb_id
                                          WHERE climb_log.user_id =` + user_id);
    }

    async get_adjusted_grade(userId, climbId) {
        const result = await Database.asyncQuery(`SELECT *
                                                  FROM climbs
                                                  WHERE climb_id = ${climbId}`)
        if (result.length === 0) return {grade: 0}
        let discipline = result[0]["category"];
        let type;
        if (result[0]["angle"] < 0) {
            type = 0;
        } else if (result[0]["angle"] > 0) {
            type = 1;
        } else {
            type = 2;
        }
        let grade = result[0]["grade"];
        const result2 = await this.calc_q(discipline, type, userId);
        let q = result2["q"];
        return ({"grade": q * grade});
    }


}

module.exports = Climbs;
